package u04lab.code

import Lists._
import Lists.List._
import u04lab.code.Course.sameTeacher
import u04lab.code.Lists.List.Cons
import Optionals.Option

trait Student {
  def name: String
  def year: Int
  def enrolling(course: Course*): Unit // the student participates to a Course
  def courses: List[String] // names of course the student participates to
  def hasTeacher(teacher: String): Boolean // is the student participating to a course of this teacher?
}

trait Course {
  def name: String
  def teacher: String
}

object Course {

  def apply(name: String, teacher: String): Course = Course(name, teacher)

  object sameTeacher {
    def unapply(courses: List[Course]): Option[String] =
      sameElementOnList(map(courses)(c => c.teacher))
  }

}


case class CourseImpl(name: String, teacher: String) extends Course {

  def commonTeacher(courses: List[Course]): Unit = courses match {
    case sameTeacher()  => println("Courses have a common teacher: " + teacher)
    case _ =>  println("Courses don't have a common teacher")
  }

}

object Student {
  def apply(name: String, year: Int = 2017): Student = Student(name, year)
}


case class StudentImpl(name: String, year: Int=2017) extends Student {

  private var coursesList: List[Course]= Nil()

  override def enrolling(myCourses: Course*): Unit =  for(c<-myCourses)( coursesList = List.append(Cons(c, Nil()), coursesList))

  override def courses: List[String] = map(coursesList)(c=>c.name)

  override def hasTeacher(teacher: String): Boolean = contains(map(coursesList)(c=>c.teacher))(teacher)
}



object Try extends App {
  val cPPS = CourseImpl("PPS","Viroli")
 // println(cPPS.toString)
  val cPCD = CourseImpl("PCD","Ricci")
 // println(cPCD.toString)
  val cSDR = CourseImpl("SDR","D'Angelo")
 // println(cSDR.toString)
  val s1 = StudentImpl("mario",2015)
 // println(s1.toString)
  val s2 = StudentImpl("gino",2016)
 // println(s2.toString)
  val s3 = StudentImpl("rino") //defaults to 2017
 // println(s3.toString)
  s1.enrolling(cPPS, cPCD)
  //s1.enrolling(cPCD)
  //println(s1.courses.toString)
 /* s2.enrolling(cPPS)
  s3.enrolling(cPPS)
  s3.enrolling(cPCD)
  s3.enrolling(cSDR)
  println(s1.courses, s2.courses, s3.courses) // (Cons(PCD,Cons(PPS,Nil())),Cons(PPS,Nil()),Cons(SDR,Cons(PCD,Cons(PPS,Nil()))))
  println(s1.hasTeacher("Ricci")) // true */

  val courses = List.Cons[Course](cPPS, Cons(cPCD, Cons(cSDR, Nil())))
  println(cPPS.commonTeacher(courses))



}

/** Hints:
  * - simply implement Course, e.g. with a case class
  * - implement Student with a StudentImpl keeping a private Set of courses
  * - try to implement in StudentImpl method courses with map
  * - try to implement in StudentImpl method hasTeacher with map and find
  * - check that the two println above work correctly
  * - refactor the code so that method enrolling accepts a variable argument Course*
  */
