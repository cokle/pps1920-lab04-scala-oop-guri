package u04lab.code

import Lists.List
import Lists.List._
import u04lab.code.Course.sameTeacher
import u04lab.code.Optionals.Option

  /** Esercizio 4.1 */
  object MyList {
    def apply[A](elems: A*): List[A] = {
      var list: List[A] = Nil();
      for(i <- elems.length-1 to 0 by -1) { list =  List.append(Cons(elems(i), Nil()), list)}
      list
    }
  }

  /** Esercizio 4.2 */
  object Course {
    def apply(name: String, teacher: String): Course = Course(name, teacher)

    object sameTeacher {
      def unapply(courses: List[Course]): Option[String] =
        sameElementOnList(map(courses)(c => c.teacher))
    }
  }

  case class CourseImpl(name: String, teacher: String) extends Course {
    def commonTeacher(courses: List[Course]): Unit = courses match {
      case sameTeacher()  => println("Courses have a common teacher: " + teacher)
      case _ =>  println("Courses don't have a common teacher")
    }
  }


