package u04lab.code

import java.util.Random

import Optionals._
import Optionals.Option._
import Lists._
import u04lab.code.Streams.Stream.{Cons, cons, generate, iterate}
import u04lab.code.Streams.Stream
import u04lab.code.Streams._

trait PowerIterator[A] {
  def next(): Option[A]
  def allSoFar(): List[A]
  def reversed(): PowerIterator[A]

}

trait PowerIteratorsFactory {
  def incremental(start: Int, successive: Int => Int): PowerIterator[Int]
  def fromList[A](list: List[A]): PowerIterator[A]
  def randomBooleans(size: Int): PowerIterator[Boolean]
}

class PowerIteratorsFactoryImpl extends PowerIteratorsFactory {

  private def fromStream[A](stream: ()=>Stream[A]): PowerIterator[A] = {
     new PowerIterator[A] {

        private var index = 0;
        private lazy val elemStream = stream()

        override def next(): Option[A] =  { index = index+1; getElemOnIndex(elemStream)(index) }

        override def allSoFar(): List[A] = Stream.toList(Stream.take(elemStream)(index))

        override def reversed(): PowerIterator[A] = fromList(List.reverse(allSoFar()))

        private def getElemOnIndex(str:  Stream[A])(index: Int): Option[A] = {
            Stream.drop(Stream.take(str)(index))(index-1) match {
              case Cons(head, _) => Some(head())
              case _ => None()
            }
        }
     }
  }

  override def incremental(start: Int, successive: Int => Int): PowerIterator[Int] =  fromStream(() => Stream.iterate(start)(successive))

  override def fromList[A](list: List[A]): PowerIterator[A] = fromStream(() => List.toStream(list))

  override def randomBooleans(size: Int): PowerIterator[Boolean] = {
    val rand = new Random();
    fromStream[Boolean](() => Stream.take(Stream.generate(rand.nextBoolean()))(size))
  }


}