package u04lab.code

import Optionals._
import Lists._
import Lists.List._
import Streams.Stream



import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions._


class ListFactoriesTest {

  var list: List[Integer] = Nil();
  @Test
  def createListWithSeqElemTest(): Unit ={
      list = MyList(10, 20, 30)
      assertEquals(Cons(10, Cons(20, Cons(30, Nil()))), list)
  }

}
